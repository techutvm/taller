var customersObtenidos;

function getCustomers (){
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function()
  {
    if (this.readyState == 4 && this.status == 200) {
      console.log (request.responseText);
      customersObtenidos = request.responseText;
      procesarCustomers();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarCustomers()
{
  var JSONCustomers = JSON.parse(customersObtenidos);
  var divTabla = document.getElementById("divTablaCustomers");
  var tabla = document.createElement("table");
  tabla.classList.add("table");
  tabla.classList.add("col-lg-12");
  tabla.classList.add("table-striped");

  var cabeceraNom = document.createElement("th");
  cabeceraNom.innerText = "Nombre";
  var cabeceraDir = document.createElement("th");
  cabeceraDir.innerText = "Direccion";
  var cabeceraPais = document.createElement("th");
  cabeceraPais.innerText = "Pais";
  var cabeceraFlag = document.createElement("th");
  cabeceraFlag.innerText = "Bandera";

  tabla.appendChild(cabeceraNom);
  tabla.appendChild(cabeceraDir);
  tabla.appendChild(cabeceraPais);
  tabla.appendChild(cabeceraFlag);



  for (var i = 0; i < JSONCustomers.value.length; i++) {
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONCustomers.value[i].ContactName;

    var columnaDireccion = document.createElement("td");
    columnaDireccion.innerText = JSONCustomers.value[i].Address;

    var columnaPais = document.createElement("td");
    columnaPais.innerText = JSONCustomers.value[i].Country;

    var columnaFlag = document.createElement("td");
    var imgFlag = document.createElement("img");
    imgFlag.classList.add("flag");
    if (JSONCustomers.value[i].Country == "UK") {
      imgFlag.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png";
    }
    else {
      imgFlag.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONCustomers.value[i].Country+".png";
    }


    columnaFlag.appendChild(imgFlag);
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaDireccion);
    nuevaFila.appendChild(columnaPais);
    nuevaFila.appendChild(columnaFlag);

    tabla.appendChild(nuevaFila);

    // console.log(JSONProductos.value[i].ProductName);
  }

  divTabla.appendChild(tabla);
}
